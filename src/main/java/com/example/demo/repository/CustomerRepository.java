package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long>{
	
	@Query(value = "SELECT * FROM customer WHERE is_delete = true ORDER BY name", nativeQuery = true)
	List<Customer> findByCustomer();
	
	@Query(value = "SELECT * FROM customer WHERE is_delete = true AND LOWER(name) LIKE LOWER(CONCAT(?1 ,'%')) ORDER BY name ", nativeQuery = true)
	List<Customer> searchByKeyword(String name);
}
