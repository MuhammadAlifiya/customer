package com.example.demo.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Customer;
import com.example.demo.repository.CustomerRepository;

@RestController
@RequestMapping("/master/")
public class ApiCustomerController {
	
	@Autowired
	private CustomerRepository customerRepository;

	@GetMapping("customer")
	private ResponseEntity<List<Customer>> getAllCustomer(){
		try {
			List<Customer> listCustomerRelation = this.customerRepository.findByCustomer();
			return new ResponseEntity<>(listCustomerRelation, HttpStatus.OK);
		}catch(Exception e){
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("customer/add")
	public ResponseEntity<Object> saveCustomer(@RequestBody Customer customer) {		
		customer.setCreatedBy("admin1");
		customer.setCreatedOn(new Date());
		customer.setIsDelete(true);
		
		Customer customerData = this.customerRepository.save(customer);

		if (customerData.equals(customer)) {
			return new ResponseEntity<>("Save Data Succes", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("customer/{id}")
	public ResponseEntity<List<Customer>> getCustomerById(@PathVariable("id") Long id) {
		try {
			Optional<Customer> customerData = this.customerRepository.findById(id);
			if (customerData.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(customerData, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<Customer>>(HttpStatus.NO_CONTENT);
		}
	}
	
	
	@PutMapping("customer/delete/{id}")
	public ResponseEntity<Object> deleteCustomerRelation(@PathVariable("id") Long id) {
		Optional<Customer> customerData = this.customerRepository.findById(id);

		if (customerData.isPresent()) {
			Customer customer = new Customer();
			customer.setId(id);
			customer.setDeletedBy("Admin3");
			customer.setDeletedOn(new Date());
			customer.setIsDelete(false);
			customer.setCreatedBy(customerData.get().getCreatedBy());
			customer.setCreatedOn(customerData.get().getCreatedOn());
			customer.setModifiedBy(customerData.get().getModifiedBy());
			customer.setModifiedOn(customerData.get().getModifiedOn());
			customer.setName(customerData.get().getName());
			customer.setDob(customerData.get().getDob());
			customer.setGender(customerData.get().getGender());
			customer.setPhoneNumber(customerData.get().getPhoneNumber());
			
			this.customerRepository.save(customer);
			
			return new ResponseEntity<>("Delete Succes", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PutMapping("customer/edit/{id}")
	public ResponseEntity<Object> editCategory(@PathVariable("id") Long id, @RequestBody Customer customer) {
		Optional<Customer> customerData = this.customerRepository.findById(id);

		if (customerData.isPresent()) {
			customer.setId(id);
			customer.setModifiedBy("Admin2");
			customer.setModifiedOn(new Date());
			customer.setCreatedBy(customerData.get().getCreatedBy());
			customer.setCreatedOn(customerData.get().getCreatedOn());
			customer.setDeletedBy(customerData.get().getDeletedBy());
			customer.setDeletedOn(customerData.get().getDeletedOn());
			customer.setIsDelete(customerData.get().getIsDelete());
			
			this.customerRepository.save(customer);
			return new ResponseEntity<Object>("Update Succes", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@GetMapping("customer/search")
	public ResponseEntity<List<Customer>> searchCustomer(@RequestParam("keyword") String keyword) {
		List<Customer> listCustomer = this.customerRepository.searchByKeyword(keyword);

		return new ResponseEntity<>(listCustomer, HttpStatus.OK);

	}
	
}
